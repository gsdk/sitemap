<?php

namespace Gsdk\Sitemap\Dom;

use DateTimeInterface;
use Gsdk\Sitemap\Enum\ChangefreqEnum;
use Gsdk\Sitemap\Support\Dom\AbstractCollection;
use Gsdk\Sitemap\Support\Dom\CommonFactory;

class Urlset extends AbstractCollection
{
    protected string $tag = 'urlset';

    protected array $tagAttributes = [
        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd',
        'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
        'xmlns:xhtml' => 'http://www.w3.org/1999/xhtml'
    ];

    public function addUrl(
        string|Url|array $loc,
        string|DateTimeInterface|null $lastmod = null,
        string|int|float|null $priority = null,
        string|ChangefreqEnum|null $changefreq = null
    ): Urlset {
        if ($loc instanceof Url) {
            $url = $loc;
        } elseif (is_array($loc)) {
            $url = Url::createFromArray($loc);
        } else {
            $url = new Url();
            $url->loc = $loc;
            $url->lastmod = CommonFactory::buildLastmod($lastmod);
            $url->priority = CommonFactory::formatPriority($priority);
            $url->changefreq = CommonFactory::buildChangefreq($changefreq);
        }

        return $this->add($url);
    }
}
