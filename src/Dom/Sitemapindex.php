<?php

namespace Gsdk\Sitemap\Dom;

use DateTimeInterface;
use Gsdk\Sitemap\Support\Dom\AbstractCollection;

class Sitemapindex extends AbstractCollection
{
    protected string $tag = 'sitemapindex';

    protected array $tagAttributes = ['xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9'];

    public function addSitemap(
        string|array|Sitemap $loc,
        string|DateTimeInterface|null $lastmod = null
    ): Sitemapindex {
        if ($loc instanceof Sitemap) {
            $sitemap = $loc;
        } elseif (is_array($loc)) {
            $sitemap = Sitemap::createFromArray($loc);
        } else {
            $sitemap = new Sitemap();
            $sitemap->loc = $loc;
            $sitemap->lastmod = $lastmod;
        }

        return $this->add($sitemap);
    }
}
