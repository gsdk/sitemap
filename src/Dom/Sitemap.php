<?php

namespace Gsdk\Sitemap\Dom;

use DateTimeInterface;
use Gsdk\Sitemap\Support\Dom\AbstractParent;
use Gsdk\Sitemap\Support\Dom\CommonFactory;

/**
 * @property string $loc
 * @property DateTimeInterface $lastmod
 */
class Sitemap extends AbstractParent
{
    protected string $tag = 'sitemap';

    public static function createFromArray(array $data): Sitemap
    {
        $sitemap = new Sitemap();
        $sitemap->loc = (string)$data['loc'];
        $sitemap->lastmod = CommonFactory::buildLastmod($data['lastmod'] ?? null);

        return $sitemap;
    }

    protected function init(): void
    {
        $this
            ->addTag('loc')
            ->addTag('lastmod');
    }
}
