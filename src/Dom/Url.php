<?php

namespace Gsdk\Sitemap\Dom;

use DateTimeInterface;
use Gsdk\Sitemap\Enum\ChangefreqEnum;
use Gsdk\Sitemap\Support\Dom\AbstractParent;
use Gsdk\Sitemap\Support\Dom\CommonFactory;
use Gsdk\Sitemap\Support\Dom\Tag;

/**
 * @property string $loc
 * @property DateTimeInterface $lastmod
 * @property ChangefreqEnum $changefreq
 * @property float $priority
 */
class Url extends AbstractParent
{
    protected string $tag = 'url';

    public static function createFromArray(array $data): Url
    {
        $url = new Url();
        $url->loc = (string)$data['loc'];
        $url->lastmod = CommonFactory::buildLastmod($data['lastmod'] ?? null);
        $url->priority = CommonFactory::formatPriority($data['priority'] ?? null);
        $url->changefreq = CommonFactory::buildChangefreq($data['changefreq'] ?? null);

        return $url;
    }

    protected function init(): void
    {
        $this
            ->addTag('loc')
            ->addTag('lastmod')
            ->addTag('changefreq')
            ->addTag('priority');
    }

    public function addAlternate(string $hreflang, string $href): void
    {
        $this->tags[] = new Tag('xhtml:link', [
            'rel' => 'alternate',
            'hreflang' => $hreflang,
            'href' => $href
        ]);
    }
}
