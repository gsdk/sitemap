<?php

namespace Gsdk\Sitemap\Support\Dom;

class Tag extends AbstractXml
{
    protected string $value;

    protected array $attributes;

    public function __construct($tag, array $attributes = [])
    {
        $this->tag = $tag;
        $this->attributes = $attributes;
    }

    public function setValue(string $value): void
    {
        switch ($this->tag) {
            case 'lastmod':
                $value = $value ? substr($value, 0, 10) : null;
                break;
            case 'priority':
                $value = (float)$value;
                if ($value <= 0 || $value > 1) {
                    $value = null;
                }
                break;
        }
        $this->value = $value;
    }

    public function isEmpty(): bool
    {
        return null === $this->value && empty($this->attributes);
    }

    public function __toString(): string
    {
        $s = '<' . $this->tag;

        foreach ($this->attributes as $k => $v) {
            $s .= ' ' . $k . '="' . $v . '"';
        }

        if (null === $this->value) {
            $s .= '>';
        } else {
            $s .= '>' . self::escape($this->value) . '</' . $this->tag . '>';
        }

        return $s;
    }
}
