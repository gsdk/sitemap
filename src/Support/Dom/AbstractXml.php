<?php

namespace Gsdk\Sitemap\Support\Dom;

abstract class AbstractXml
{
    const NL = "\n";

    protected string $tag;

    protected array $tagAttributes = [];

    protected static function escape(string $value): string
    {
        return $value
            ? str_replace(['&', '\'', '"', '>', '<'], ['&amp;', '&apos;', '&quot;', '&gt;', '&lt;'], $value)
            : '';
    }

    protected static function format_date($date): string
    {
        return substr($date, 0, 10);
    }

    protected static function tab($n = 0): string
    {
        return str_pad('', $n, "\t");
    }
}