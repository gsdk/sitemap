<?php

namespace Gsdk\Sitemap\Support\Dom;

abstract class AbstractCollection extends AbstractXml
{
    protected string $tag;

    protected array $items = [];

    protected function add(AbstractParent $item): static
    {
        $this->items[] = $item;

        return $this;
    }

    public function out(): void
    {
        header('Content-type: text/xml; charset="UTF-8"');
        echo $this->__toString();
    }

    public function __toString(): string
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . self::NL;

        $xml .= '<' . $this->tag;
        if ($this->tagAttributes) {
            foreach ($this->tagAttributes as $k => $v) {
                $xml .= ' ' . $k . '="' . $v . '"';
            }
        }
        $xml .= '>' . self::NL;

        foreach ($this->items as $item) {
            $xml .= $item;
        }

        $xml .= '</' . $this->tag . '>';

        return $xml;
    }
}
