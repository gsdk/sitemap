<?php

namespace Gsdk\Sitemap\Support\Dom;

use DateTime;
use DateTimeInterface;
use Gsdk\Sitemap\Enum\ChangefreqEnum;

class CommonFactory
{
    public static function buildLastmod(string|DateTimeInterface|null $lastmod): ?DateTimeInterface
    {
        if (is_null($lastmod)) {
            return null;
        } elseif (is_string($lastmod)) {
            return new DateTime($lastmod);
        } else {
            return $lastmod;
        }
    }

    public static function formatPriority(string|int|float|null $priority): ?float
    {
        if (is_null($priority)) {
            return null;
        }

        return (float)$priority;
    }

    public static function buildChangefreq(string|ChangefreqEnum|null $changefreq): ?ChangefreqEnum
    {
        if (is_null($changefreq)) {
            return null;
        } elseif (is_string($changefreq)) {
            return ChangefreqEnum::from($changefreq);
        } else {
            return $changefreq;
        }
    }
}