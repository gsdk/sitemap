<?php

namespace Gsdk\Sitemap\Support\Dom;

abstract class AbstractParent extends AbstractXml
{
    protected string $tag;

    protected array $tags = [];

    public function __construct()
    {
        $this->init();
    }

    abstract protected function init(): void;

    public function __set($name, $value)
    {
        if (!isset($this->tags[$name])) {
            return;
        }

        $this->tags[$name]->setValue($value);
    }

    public function addTag(string $tag): static
    {
        $this->tags[$tag] = new Tag($tag);

        return $this;
    }

    public function __toString(): string
    {
        $xml = self::tab(1) . '<' . $this->tag . '>' . self::NL;
        foreach ($this->tags as $tag) {
            if ($tag->isEmpty()) {
                continue;
            }
            $xml .= self::tab(2) . (string)$tag . self::NL;
        }
        $xml .= self::tab(1) . '</' . $this->tag . '>' . self::NL;

        return $xml;
    }
}
