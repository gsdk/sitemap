<?php

namespace Gsdk\Sitemap;

use Gsdk\Sitemap\Builder\SitemapindexBuilder;
use Gsdk\Sitemap\Builder\UrlsetBuilder;

class SitemapGenerator
{
    public static function makeSitemapindex(): SitemapindexBuilder
    {
        return new SitemapindexBuilder();
    }

    public static function makeUrlset(): UrlsetBuilder
    {
        return new UrlsetBuilder();
    }
}