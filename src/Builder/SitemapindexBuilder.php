<?php

namespace Gsdk\Sitemap\Builder;

use DateTimeInterface;
use Gsdk\Sitemap\Dom\Sitemap;
use Gsdk\Sitemap\Dom\Sitemapindex;

class SitemapindexBuilder
{
    private Sitemapindex $sitemapindex;

    public function __construct()
    {
        $this->sitemapindex = new Sitemapindex();
    }

    public function addSitemap(
        string|array|Sitemap $loc,
        string|DateTimeInterface|null $lastmod = null
    ): static {
        $this->sitemapindex->addSitemap($loc, $lastmod);

        return $this;
    }

    public function build(): Sitemapindex
    {
        return $this->sitemapindex;
    }
}