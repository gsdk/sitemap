<?php

namespace Gsdk\Sitemap\Builder;

use DateTimeInterface;
use Gsdk\Sitemap\Dom\Url;
use Gsdk\Sitemap\Dom\Urlset;
use Gsdk\Sitemap\Enum\ChangefreqEnum;

class UrlsetBuilder
{
    private Urlset $urlset;

    public function __construct()
    {
        $this->urlset = new Urlset();
    }

    public function addUrl(
        string|Url|array $loc,
        string|DateTimeInterface|null $lastmod = null,
        string|int|float|null $priority = null,
        string|ChangefreqEnum|null $changefreq = null
    ): static {
        $this->urlset->addUrl($loc, $lastmod, $priority, $changefreq);

        return $this;
    }

    public function build(): Urlset
    {
        return $this->urlset;
    }
}