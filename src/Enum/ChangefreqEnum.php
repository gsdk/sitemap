<?php

namespace Gsdk\Sitemap\Enum;

enum ChangefreqEnum: string
{
    case ALWAYS = 'always';
    case hourly = 'hourly';
    case daily = 'daily';
    case weekly = 'weekly';
    case monthly = 'monthly';
    case yearly = 'yearly';
    case never = 'never';
}